echo off

rmdir bin\temp\archive
mkdir bin\temp\archive

bin\curl\curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{\"options\": {\"packageName\": \"SYBON.Archive\",\"packageVersion\": \"0.0.1\",\"sourceFolder\": \"src\"}, \"swaggerUrl\": \"http://archive.sybon.org/swagger/v1/swagger.json\"}" "http://generator.swagger.io/api/gen/clients/csharp" | bin\jq .link > bin\temp\archive\link

set /p link=<bin\temp\archive\link

bin\curl\curl %link% --output bin\temp\archive\archive-lib.zip

bin\unzip -o "bin\temp\archive\archive-lib.zip" -d "bin\temp\archive"

start libs\csharp\bin\archive\compile.cmd
