echo off

rmdir bin\temp\auth
mkdir bin\temp\auth

bin\curl\curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{\"options\": {\"packageName\": \"SYBON.Auth\",\"packageVersion\": \"0.0.1\",\"sourceFolder\": \"src\"}, \"swaggerUrl\": \"http://auth.sybon.org/swagger/v1/swagger.json\"}" "http://generator.swagger.io/api/gen/clients/csharp" | bin\jq .link > bin\temp\auth\link

set /p link=<bin\temp\auth\link

bin\curl\curl %link% --output bin\temp\auth\auth-lib.zip

bin\unzip -o "bin\temp\auth\auth-lib.zip" -d "bin\temp\auth"

start libs\csharp\bin\auth\compile.cmd
