echo off

rmdir bin\temp\checking
mkdir bin\temp\checking

bin\curl\curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{\"options\": {\"packageName\": \"SYBON.Checking\",\"packageVersion\": \"0.0.1\",\"sourceFolder\": \"src\"}, \"swaggerUrl\": \"http://checking.sybon.org/swagger/v1/swagger.json\"}" "http://generator.swagger.io/api/gen/clients/csharp" | bin\jq .link > bin\temp\checking\link

set /p link=<bin\temp\checking\link

bin\curl\curl %link% --output bin\temp\checking\checking-lib.zip

bin\unzip -o "bin\temp\checking\checking-lib.zip" -d "bin\temp\checking"

start libs\csharp\bin\checking\compile.cmd
